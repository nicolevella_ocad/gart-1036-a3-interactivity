/*
  Project:      A3: Intereactivity
  Student:      Nicole Vella
  Professor:    Geoffrey Shea
  Course:       GART 1036 - Art & Code - OCAD University
  Created on:   January 25, 2020
  Based on:     Example code from Processing Sound Library (PeakAmplitude.pde)
                Example code from Leap Motion Library (LM_1_Basics)
                
  Video Demo:   https://vimeo.com/387178286
*/

// import Leap Motion and Sound libraries
import de.voidplus.leapmotion.*;
import processing.sound.*;

// decclare LeapMotion, Amplitude, and SoundFile classes from libraries 
LeapMotion leap;
Amplitude amplitude;
SoundFile songInput;

// variables to store finger tip position, and pinch/grab gesture values
PVector fingerTipPos;
float handGrab;
float handPinch;

// initial size and color of circles which make up blob
int circSize = 16;
int circCol = 0;


void setup() {

  // define new instanaces of leap, amplitude, and songinput classes declared above
  leap = new LeapMotion(this);
  amplitude = new Amplitude(this);
  songInput = new SoundFile(this, "070shake_nice2have.mp3");

  // loop the song incase anyone wants to play for a long time
  songInput.loop();

  // connect the song to the amplitude instance
  amplitude.input(songInput);

  // setup the canvas
  colorMode(HSB, 360, 100, 100);
  frameRate(24);
  background(0, 100, 0);
  size(1920, 1080);

  // no strokes so that the circles appear as a blob
  noStroke();
}

void draw() {

  // variable to hold the current volume of the song, returns value from 0 to 1
  float volume = amplitude.analyze();

  // var to map the volume level to the size of the circles/blob
  circSize = round(map(volume, 0, 1, 10, 300));

  // Hand and Finger are special classes inside the LeapMotion library, when the camera
  // detects a hand or finger in the frame, the code within will execute
  for (Hand hand : leap.getHands ()) {

    // get the pinch and grab values, returns values from 0 to 1
    handPinch = hand.getPinchStrength();
    handGrab = hand.getGrabStrength();

    // if the hand is "fully grabbed" (making a fist),
    // clear the bg on each draw so we have a floating blob
    // also change it's hue by 1 value on every draw()
    if (handGrab >=0.97) {
      circCol += circCol;
      background(0, 100, 0);
      fill(circCol, 100, 100);
    }

    // when fingers are detected on the screen
    for (Finger finger : hand.getFingers()) {

      // get the position of each finger tip, returns vector point
      fingerTipPos = finger.getPosition();

      // var to map the pinch level to the hue of the circles/blob
      circCol = round(map(handPinch, 0, 1, 0, 360));
      fill(circCol, 100, 100);
      
      // draw the circles/blob at the finger tip points
      // at a size relative to the volume of the audio being played
      circle(fingerTipPos.x, fingerTipPos.y, circSize);
    }
  }
}
